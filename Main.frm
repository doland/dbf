VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form Main 
   Caption         =   "Main"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Timer TimerMUMPSConnect 
      Enabled         =   0   'False
      Left            =   360
      Top             =   960
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   840
      Top             =   360
      _ExtentX        =   741
      _ExtentY        =   741
   End
   Begin VB.Timer Timer1 
      Interval        =   5000
      Left            =   360
      Top             =   360
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const adUseNone = 1
Const adUseServer = 2

Const adOpenUnspecified = -1
Const adOpenForwardOnly = 0
Const adOpenKeyset = 1
Const adOpenDynamic = 2

Const adUseClient = 3
Const adOpenStatic = 3

Const adLockReadOnly = 1
Const adLockOptimistic = 3
Const adLockPessimistic = 2
Const adLockBatchOptimistic = 4
Const adLockUnspecified = -1

Dim startTime, Param As String
Dim neighborCol As Collection
Dim comp As comp
Dim lngTimer As Long
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long

Private Sub Form_Load()
    
    Timer1.Enabled = False
    TimerMUMPSConnect = False
    TimerMUMPSConnect.Interval = 60000
    DoEvents
    IniFile
    loadSettings
    Timer1.Interval = 60000
    If Timer1min > 0 Then
        Timer1.Enabled = True
    End If
End Sub

Private Function readTab(tabN As String, comp As comp) As Collection
    On Error GoTo errHandler
    Dim c As Object
    Set readTab = New Collection
    
    If PathFileExists(comp.dbPath & tabN & ".dbf") = 0 Then
        comp.report = "ERR:" & tabN & " �� �������|"
        Exit Function
    End If
    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & comp.dbPath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Set Record = CreateObject("ADODB.Recordset")
    Record.CursorLocation = adUseClient
    
    sqlQuery = "SELECT * FROM " & tabN
    Record.Open sqlQuery, c, adOpenStatic, adLockReadOnly
    foundEmptyRecords = 0
    If Record.RecordCount > 0 Then
        
        Record.MoveFirst
                
        Do Until Record.EOF
            
            Set itemRecord = New Collection
            flIsNull = True
            For i = 0 To Record.Fields.Count - 1 Step 1
                Set field = New field
                field.fName = Record.Fields.Item(i).Name
                If Not IsNull(Record.Fields.Item(i).Value) Then
                    field.fValue = Record.Fields.Item(i).Value
                    flIsNull = False
                End If
                itemRecord.Add field
            Next
            
            If flIsNull Then foundEmptyRecords = foundEmptyRecords + 1
            
            readTab.Add itemRecord, CStr(Record.AbsolutePosition)
            Record.MoveNext
        Loop
    End If
    comp.report = "NULL:" & CStr(foundEmptyRecords) & "|"
    
    Exit Function
errHandler:
    Set Record = Nothing: Set c = Nothing
    LogAction "readTab|" & Err.Number & " " & Err.Description, True
End Function

Private Sub check(tabName As String)
    Dim kolvoFind As Integer
    Dim flDouble, flEquils As String
    
    ' �������� ���������� ������
    For Each comp In neighborCol
       Set comp.tabCollection = readTab(CStr(tabName), comp)
    Next

    flDouble = Piece(Param, ":", 2)
    ' �������� �� ��������� �����
    For Each comp In neighborCol
        If Piece(comp.report, ":", 1) = "ERR" Then GoTo me1:
        
        kolvoFind = 0
        If flDouble = "0" Then GoTo m5
        For ind1 = 1 To comp.tabCollection.Count
            Set recordItem1 = comp.tabCollection.Item(ind1)
            DoEvents
            For ind2 = ind1 + 1 To comp.tabCollection.Count
                
                Set recordItem2 = comp.tabCollection.Item(ind2)
                fieldsAmount = recordItem1.Count
                For Each field1 In recordItem1
                    
                    For Each field2 In recordItem2
                        If field1.fName = field2.fName Then
                            If field1.fValue = field2.fValue Then fieldsAmount = fieldsAmount - 1
                            GoTo m1
                        End If
                    Next

m1:             Next
                If fieldsAmount <= 0 Then
                    kolvoFind = kolvoFind + 1
                End If
            Next
        Next
        ' ������ � ������ ���������� ����������
m5:     If flDouble = "0" Then rep = "NOT" Else rep = CStr(kolvoFind)
        comp.report = comp.report & "DOUBLE:" & rep & "|"
me1: Next
    
    flEquals = Piece(Param, ":", 3)
    ' ������ �������� ��� �� ������ ���������
    For Each comp1 In neighborCol
        If Piece(comp1.report, ":", 1) = "ERR" Then GoTo me2
        Set tabCollection1 = comp1.tabCollection
        
        For Each comp2 In neighborCol
            If Piece(comp2.report, ":", 1) = "ERR" Then GoTo m3
            If flEquals = "0" Then GoTo m6
            DoEvents
            If comp1.Ip = comp2.Ip Then GoTo m3 ' ���� ��� ��� �� ����, ����������
            
            Set tabCollection2 = comp2.tabCollection
            totalRecords = tabCollection1.Count
            
            For Each recordItem1 In tabCollection1
                For ind = 1 To tabCollection2.Count
                    Set recordItem2 = tabCollection2.Item(ind)
                    
                    fieldsAmount = recordItem1.Count
                    
                    For Each field1 In recordItem1
                        
                        For Each field2 In recordItem2
                            If field1.fName = field2.fName Then
                                If field1.fValue = field2.fValue Then
                                    fieldsAmount = fieldsAmount - 1
                                    GoTo m2
                                End If
                            End If
                        Next
                        
m2:                 Next

                    If fieldsAmount = 0 Then
                        totalRecords = totalRecords - 1
                        tabCollection2.Remove ind
                        ' ����� ����������� ������, ����� ������ ���������
                        GoTo m4
                    End If
                Next
m4:         Next
            ' ������ � ������ ���������� ������������� �������
m6:         If flEquals = "0" Then rep = "NOT" Else rep = CStr(totalRecords)
            comp1.report = comp1.report & comp2.Ip & ":" & "NOTEQUALS:" & rep & "$"
m3:     Next
me2: Next
    
    For Each comp In neighborCol
        AddToTextFile "report.txt", "TAB:" & tabName & "|MyIP:" & comp.Ip & "|" & comp.report
    Next
    Set neighborCol = Nothing
End Sub

Sub getNeighbors()
    Dim lastNum As Integer, neighborIP As String, neighbor As comp
    Set neighborCol = New Collection
    Ip = Winsock1.LocalIP
    
    lastNum = CInt(Piece(CStr(Ip), ".", 4))
 
    If lastNum = 14 Then
        neighborIP = Piece(CStr(Ip), ".", 1, 3) & ".15"
        'lstPath.AddItem "\\" & Piece(CStr(IP), ".", 1, 3) & ".15\d$\ExchangeFile\"
    Else 'If lastNum = 15 Then
        neighborIP = Piece(CStr(Ip), ".", 1, 3) & ".14"
    End If
        
    Set comp = New comp
    
    With comp
        .Ip = Ip
        .dbPath = "\\" & Ip & "\d$\Base\"
        .exchPath = "\\" & Ip & "\d$\ExchangeFile\"
    End With
    
    neighborCol.Add comp
        
    If Not AnybodyHere(neighborIP) Then Exit Sub
    
    Set neighbor = New comp

    
    With neighbor
        .Ip = neighborIP
        .dbPath = "\\" & neighborIP & "\d$\Base\"
        .exchPath = "\\" & neighborIP & "\d$\ExchangeFile\"
    End With
    
    neighborCol.Add neighbor
End Sub
Private Sub Timer1_Timer()
    lngTimer = lngTimer + 1
    If lngTimer < Timer1min Then Exit Sub
    lngTimer = 0
    Timer1.Enabled = False
    FileName = "C:\MClib\checker.ini"
    f = FreeFile
    
    AddToTextFile "report.txt", "TIME:" & Now()
    
    Open FileName For Input As #f
    Do While Not EOF(f)
        Line Input #f, Stroka ' ������ ������ ������
        If UCase(Piece(Stroka, "|", 1)) = "1TIMER" Then GoTo m1
        tabName = UCase(Piece(Stroka, "|", 1)): If tabName = "" Then GoTo m1
        
        Param = Piece(Stroka, "|", 2)
                
        getNeighbors
        check CStr(tabName)
        Set neighborCol = Nothing
m1: Loop
    Close #f
    sendToMUMPS
    Timer1.Enabled = True
End Sub

Private Sub sendToMUMPS()
    Dim M As Object
    On Error GoTo errHandler
    
    Set M = CreateObject("MC.Class1")
    
    Init = M.�������������(MServer, MPort, "USER", "1")
    If M.����������(MKip, MTom) = 0 Then
        LogAction "Main|sendToMUMPS|�� ������� ����������� � �������!", True
    End If
    
    TimerMUMPSConnect = False
    
    FileName = "report.txt"
    If PathFileExists(FileName) = 0 Then Exit Sub
    
    
    f = FreeFile
    Open FileName For Input As #f
    Do While Not EOF(f)
        Line Input #f, Stroka ' ������ ������ ������
        M.��������������� (Stroka)
    Loop
    Close #f
    
    ret = M.���������������("CHECKLOG^DOLGOV", 5, 5, IM, ClCode, RMNum & "|" & Winsock1.LocalIP)
    If M.MErrNum <> 0 Then
        LogAction "MAin|sendToMUMPS|���������� ����|" & M.MErrNum & "|" & M.MErrDesc, True
    Else
        If Piece(ret, "|", 1) <> "+" Then
            LogAction "MAin|sendToMUMPS|���������� ����|" & ret, True
        Else
            ' ��� ��������� ���������, ��� �������
            DeleteFile (FileName)
            Exit Sub
        End If
    End If
    Set M = Nothing
    ' ���-�� ����� �� ���, ��������� ������
    TimerMUMPSConnect = True
    Exit Sub
errHandler:
    Set M = Nothing
    TimerMUMPSConnect = True
    LogAction "Main|sendToMUMPS|" & Err.Number & "|" & Err.Description, True
End Sub

Private Sub TimerMUMPSConnect_Timer()
    sendToMUMPS
End Sub
