Attribute VB_Name = "tasks"
Public Sub getTasks(sqlQuery As String, taskCollection As Collection)
    On Error GoTo errHandler
    Dim task As task
    ' ���������� taskCollection ������������� ���������
    Dim c As Object 'New ADODB.Connection
    Set Record = CreateObject("ADODB.Recordset")
    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Record.CursorLocation = adUseClient
    
    '������� �� TASK_TAB
    Record.Open sqlQuery, c, adOpenStatic, adLockReadOnly

    If Record.RecordCount > 0 Then
        
        Record.MoveFirst
        Do Until Record.EOF
            If IsNull(Record.Fields("FIO").Value) < 0 Then '���
                Fio = ""
            Else
                Fio = Record.Fields("FIO").Value
            End If
            
            If IsNull(Record.Fields("TABNO").Value) < 0 Then '�����
                TabNo = ""
            Else
                TabNo = Record.Fields("TABNO").Value
            End If
            
            If IsNull(Record.Fields("TASKDATE").Value) < 0 Then '����
                TaskDate = ""
            Else
                TaskDate = Record.Fields("TASKDATE").Value
            End If
            
            If IsNull(Record.Fields("TASKNUM").Value) < 0 Then '�����
                taskNum = ""
            Else
                taskNum = Record.Fields("TASKNUM").Value
            End If
                        
            If IsNull(Record.Fields("TASKCODE").Value) < 0 Then '���
                TaskCode = ""
            Else
                TaskCode = Record.Fields("TASKCODE").Value
            End If
            
            If IsNull(Record.Fields("TEXT1").Value) < 0 Then
                Text1 = ""
            Else
                Text1 = Record.Fields("TEXT1").Value
            End If
            
            If IsNull(Record.Fields("TEXT2").Value) Then
                Text2 = ""
            Else
                Text2 = Record.Fields("TEXT2").Value
            End If
                        
            If IsNull(Record.Fields("TEXT3").Value) Then
                Text3 = ""
            Else
                Text3 = Record.Fields("TEXT3").Value
            End If
                        
            If IsNull(Record.Fields("TEXT4").Value) Then
                Text4 = ""
            Else
                Text4 = Record.Fields("TEXT4").Value
            End If
            
            If IsNull(Record.Fields("DOPDAN").Value) Then
                DopDan = ""
            Else
                DopDan = Record.Fields("DOPDAN").Value
            End If
                        
            If IsNull(Record.Fields("TASKEND").Value) Then
                TaskEnd = ""
            Else
                TaskEnd = Record.Fields("TASKEND").Value
            End If
            
            If IsNull(Record.Fields("CLCODE").Value) Then
                ClCode = ""
            Else
                ClCode = Record.Fields("CLCODE").Value
            End If
            
            If IsNull(Record.Fields("RMNUM").Value) Then
                RMNum = ""
            Else
                RMNum = Record.Fields("RMNUM").Value
            End If
            
            If IsNull(Record.Fields("FILENAME").Value) Then
                FileName = ""
            Else
                FileName = Record.Fields("FILENAME").Value
            End If
            
            If IsNull(Record.Fields("UNLOAD").Value) Then
                Unloaded = ""
            Else
                Unloaded = Record.Fields("Unload").Value
            End If
            Set task = New task
            With task
                .Fio = Fio
                .TabNo = TabNo
                .DT = TaskDate
                .Number = taskNum
                .Code = TaskCode
                .Text1 = Text1
                .Text2 = Text2
                .Text3 = Text3
                .Text4 = Text4
                .DopDan = DopDan
                .TaskEnd = TaskEnd
                .ClCode = ClCode
                .RMNum = RMNum
                .FileName = FileName
                .Unloaded = Unloaded
            End With
 
            taskCollection.Add task

            Record.MoveNext
        Loop
    End If
    Record.Close
    
    Exit Sub
errHandler:
    LogAction "tasks|getTasks|" & Err.Number & "|" & Err.Description, True
End Sub

