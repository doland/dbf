Attribute VB_Name = "common"
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long

Public Function FileUtils_MkDir(strPath As String) As Boolean ' ������� ������� ����� � ���������� TRUE\FALSE � ����������� �� ������ ��������.
    On Error Resume Next
    Err.Clear
    Call MkDir(strPath)
    If Err.Number = 75 Then Err.Clear
    FileUtils_MkDir = (Err.Number = 0)
End Function

' ������� ��������� ������� ����� � ���������� �����������
Public Function AnybodyHere(ByVal strComputerName As String) As Boolean

    On Error GoTo ErrorHandlerAH
    Dim strResult   As String
    Dim ShellX      As String
    Dim FileNum     As Integer
    Dim lPid        As Long
    'DoEvents
    ShellX = Shell("command.com /c ping -n 1 " & strComputerName & " > log.txt", vbHide)
    Sleep 200
    lPid = ShellX
    
    If lPid <> 0 Then
        k = 1
        While (strResult = "") And (k < 10)
            If PathFileExists("log.txt") Then
                FileNum = FreeFile
                Open "log.txt" For Input As #FileNum
                If Not EOF(FileNum) Then strResult = Input(LOF(FileNum), 2)
                Close #FileNum
            End If
            
            If strResult = "" Then Sleep 1000: k = k + 1
        Wend
               
        AnybodyHere = (InStr(DosToWin(strResult), "�������� = 0") > 0) Or (InStr(strResult, "�������� = 0") > 0)
    End If
    Exit Function
ErrorHandlerAH:
    LogAction "common|AnybodyHere|" & Err.Number & "|" & Err.Description, True
    AnybodyHere = False
    Exit Function
End Function
