Attribute VB_Name = "modDBF"
Private Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long

Public Const adUseClient = 3
Public Const adOpenStatic = 3
Public Const adLockReadOnly = 1
Public Const adLockOptimistic = 3

Public Sub MakeNewTabAndCopy(nameTAB As String, sqlStrColumns As String, path As String) '������� ����� DBF-�������, ��������� ������ �� ������
    Dim cIn, cOut As Object
    
    If Not FileUtils_MkDir(path & "tmp") Then GoTo errMakeDir
    
    Set cIn = CreateObject("ADODB.Connection")
    Set cOut = CreateObject("ADODB.Connection")

    CreateDBF nameTAB, sqlStrColumns, path & "tmp"
    
    cIn.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & "tmp" & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    
    Set rstIn = CreateObject("ADODB.Recordset")
    rstIn.CursorLocation = adUseClient
    rstIn.CursorType = adOpenStatic
    rstIn.LockType = adLockOptimistic
    rstIn.ActiveConnection = cIn
    rstIn.Open nameTAB
       
    '�������� ������ ������� � ����� �������
    strCol = "|"
    For i = 0 To rstIn.Fields.Count - 1 Step 1
        strCol = strCol & CStr(rstIn.Fields.Item(i).Name) & "|"
    Next
       
    '��������� ������ �� ������ ������� � �����
    cOut.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Set rstOut = CreateObject("ADODB.Recordset")
    rstOut.CursorLocation = adUseClient
    rstOut.Open "SELECT * FROM " & nameTAB, cOut, adOpenStatic, adLockOptimistic
      
    If rstOut.RecordCount > 0 Then
        rstOut.MoveFirst
                
        Do Until rstOut.EOF
            rstIn.AddNew
            For i = 0 To rstOut.Fields.Count - 1 Step 1
                tCol = rstOut.Fields.Item(i).Name
                
                If l(strCol, "|" & tCol & "|") > 1 Then
                    rstIn.Fields(tCol).Value = rstOut.Fields.Item(i)
                End If
            Next
                   
            rstIn.Update
                      
            rstOut.MoveNext
        Loop
    End If
    
    rstIn.Close: cIn.Close
    rstOut.Close: cOut.Close
    
    CopyFile path & "tmp\" & nameTAB & ".DBF", path & nameTAB & ".DBF", False
    DeleteFile path & "tmp\" & nameTAB & ".DBF"
    Exit Sub
errMakeDir:
    LogAction "modDBF|MakeNewTabAndCopy|������ ��� �������� ��������� ����� " & path & "tmp", True
    Exit Sub
    
End Sub

Public Function ColumnInDBF(nameTAB As String, path As String, column As String) As Boolean '��������� ���������� �� ������� � DBF-�������
    Dim c As Object
    
    column = UCase(column)
    
    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    
    ColumnInDBF = False
    
    Set rst = CreateObject("ADODB.Recordset")
    rst.CursorLocation = adUseClient
    rst.Open nameTAB, c, adOpenStatic, adLockReadOnly
    
    For i = 0 To rst.Fields.Count - 1 Step 1
        If rst.Fields.Item(i).Name = column Then ColumnInDBF = True: Exit Function
    Next
       
    ColumnInDBF = False
End Function

Public Sub CreateDBF(nameTAB As String, sqlStrColumns As String, path As String) '������� DBF �������
    Dim c As Object

    If PathFileExists(path & "\" & nameTAB & ".DBF") = 1 Then DeleteFile path & "\" & nameTAB & ".DBF"

    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & ";User ID=Admin;Password=;Extended Properties=dBase IV")
      
    sqlQuery = "create table " _
                    & nameTAB & " " _
                    & sqlStrColumns
    c.Execute sqlQuery
    c.Close
End Sub
