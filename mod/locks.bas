Attribute VB_Name = "locks"
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Public Function setLockON(lockThis As String) As Boolean
On Error GoTo ErrHandlerSetLockOn
    '1. ������� ���� ���� �� ��������� ����������
    If Not (SetLock(ExchPath & lockThis)) Then
        setLockON = False
        Exit Function ' ���� ��� ���� ���� �������
    End If
    
    ' 2. �������� ��������� ���� �� ������� ����� � ����� �� ������
    For i = 0 To MainForm.lstPath.ListCount - 1
    
        LockFile = MainForm.lstPath.List(i) & lockThis
        slaveIP = Piece(MainForm.lstPath.List(i), "\", 3)
        
        If AnybodyHere(slaveIP) Then ' ���������� ���� ������
            
            If IsLock(CStr(LockFile)) Then  ' ���� ���� �� ������ ���������- ����������
                
                LogAction "locks|AnyTask|" & CStr(LockFile) & "|���� ����, ������� �� �������"
                ' ���� ����, ������, �������� �������
                RemoveLock (ExchPath & lockThis)
                setLockON = False
                Exit Function
            End If
        End If
    Next i
    
    '3. ���������� ���������� ���������� ����� �� ����
    For i = 0 To MainForm.lstPath.ListCount - 1
        
        LockFile = MainForm.lstPath.List(i) & lockThis
        slaveIP = Piece(MainForm.lstPath.List(i), "\", 3)
        
        If AnybodyHere(slaveIP) Then ' ���������� ���� ������
            
            If SetLock(CStr(LockFile)) = False Then  ' ���� ���� �� ������ ���������- ����������
                
                LogAction "locks|AnyTask|" & CStr(LockFile) & "|������ ���������� (����)", True
                ' ���� �������� ������- ���� ������� ��� ������������ ����
                tmp = setLockOFF(CStr(LockFile))
                setLockON = False
                Exit Function
            End If
        End If
    Next i
    
    ' ��� ��������� �� ����- ��� ������, ���������� �����������, �������� ������
    setLockON = True
    Exit Function
    
ErrHandlerSetLockOn:
    setLockON = False
End Function

Public Function setLockOFF(lockThis As String) As Boolean
    On Error GoTo ErrHandlerSetLockOFF
    
    ' 1. �������� �� ���������� ���������� � ������ �� ��� ����� �����
    For i = 0 To MainForm.lstPath.ListCount - 1
    
        LockFile = MainForm.lstPath.List(i) & lockThis
        slaveIP = Piece(MainForm.lstPath.List(i), "\", 3)
        
        If AnybodyHere(slaveIP) Then ' ���������� ���� ������
            ' ������ ��� ����������
            tmp = RemoveLock(CStr(LockFile))
        End If
        
    Next i
    
    ' � ���� ����
    RemoveLock (ExchPath & lockThis)
    ' ���������� �����, �������� ������
    setLockOFF = True
    Exit Function
    
ErrHandlerSetLockOFF:
    setLockOFF = False
End Function

' ������� ���������� ������� �� ��� (���� ���� ����������)
Public Function IsLock(fullPathAndFileName As String) As Boolean
    If PathFileExists(fullPathAndFileName) = 1 Then
        IsLock = True
    Else
        IsLock = False
    End If
End Function

'������� ������� ��� (������� ����)
Public Function RemoveLock(fullPathAndFileName As String) As Boolean
    DeleteFile (fullPathAndFileName)
End Function

'������� ������ ��� (������� ����), ��� ���������� False, ���� �� ��� ����������
Public Function SetLock(FileName As String) As Boolean
    On Error GoTo ErrHandlerSetLock
LockSL:
    If IsLock(FileName) Then
'        Sleep 200
'        Kol = Kol + 1
'        If Kol < 25 Then
'            GoTo LockSL
'        End If
        SetLock = False
    Else
        f = FreeFile
        Open FileName For Output As #f
        Print #f, "1"
        Close #f
        SetLock = True
    End If
    Exit Function
ErrHandlerSetLock:
    SetLock = False
End Function

' ������� ��������� ������� ����� � ���������� �����������
Public Function AnybodyHere(ByVal strComputerName As String) As Boolean

    On Error GoTo ErrorHandlerAH
    Dim strResult   As String
    Dim ShellX      As String
    Dim FileNum     As Integer
    Dim lPid        As Long
    'DoEvents
    ShellX = Shell("command.com /c ping -n 1 " & strComputerName & " > log.txt", vbHide)
    Sleep 200
    lPid = ShellX
    If lPid <> 0 Then
'        lHnd = OpenProcess(SYNCHRONIZE, 0, lPid)
'        If lHnd <> 0 Then
'            lRet = WaitForSingleObject(lHnd, INFINITE)
'            CloseHandle (lHnd)
'        End If
        FileNum = FreeFile
        Open "log.txt" For Input As #FileNum
        strResult = Input(LOF(1), 1)
        Close #FileNum
        AnybodyHere = (InStr(DosToWin(strResult), "�������� = 0") > 0) Or (InStr(strResult, "�������� = 0") > 0)
    End If
    Exit Function
ErrorHandlerAH:
    LogAction "locks|AnybodyHere|" & Err.Number & "|" & Err.Description, True
    AnybodyHere = False
    Exit Function
End Function

Public Sub DeleteRemoteLocks(Index As Integer)
    For i = 0 To Index
        RemoveLock MainForm.lstPath.List(i) & "Lock.txt"
    Next i
End Sub

