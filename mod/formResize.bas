Attribute VB_Name = "formResize"
' by doland
Public Sub WidthHeight(Element As Object, WidthScale As Double, HeightScale As Double)
    On Error Resume Next
    Element.Left = Element.Left * WidthScale
    On Error Resume Next
    Element.Top = Element.Top * HeightScale
    On Error Resume Next
    Element.Width = Element.Width * WidthScale
    On Error Resume Next
    Element.Height = Element.Height * HeightScale
    'On Error Resume Next
    'Element.Font.Size = Element.Font.Size * WidthScale
    On Error Resume Next
    Element.X1 = Element.X1 * WidthScale
    On Error Resume Next
    Element.X2 = Element.X2 * WidthScale
    On Error Resume Next
    Element.Y1 = Element.Y1 * WidthScale
    On Error Resume Next
    Element.Y2 = Element.Y2 * WidthScale
End Sub
