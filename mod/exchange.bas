Attribute VB_Name = "exchange"
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long
Private Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long

Public Sub Sinchronize()
    '��������� ����� ��������� ���� �� ����� ��� ������ �� ������ ��������� � ������ �� ����
    '������� ��������� ����� �������� ������ � ������ �����
    Dim Found As Boolean, Kol As Integer, LockFile As String, i As Integer, CanDelete As Boolean
    LogAction "exchange|Sinchronize|������ �������������"
    
    If (MainForm.lstPath.ListCount < 1) Or (KolComputers = 0) Then
        LogAction "exchange|Sinchronize|��� ���������� ��� ��������"
        Exit Sub
    End If
    If SetLock(ExchPath & "Lock.txt") = False Then
        LogAction "exchange|Sinchronize|�� ������� ���������� ��� �� Lock.txt"
        Exit Sub
    End If
    
    For i = 0 To MainForm.lstPath.ListCount - 1
        Kol = 0
        LockFile = MainForm.lstPath.List(i) & "Lock.txt"
        If AnybodyHere(Piece(MainForm.lstPath.List(i), "\", 3)) Then
            If SetLock(LockFile) = False Then
                DeleteRemoteLocks i
                RemoveLock (ExchPath & "Lock.txt")
                Exit Sub
            End If
        Else
            RemoveLock (ExchPath & "Lock.txt")
            LogAction "exchange|Sinchronize|��� ��������� � ����|" & LockFile
            Exit Sub
        End If
        
        IsCopy = CopyFile(MainForm.lstPath.List(i) & OutputFile, ExchPath & i & OutputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
        If IsCopy <> 1 Then '��� ������ ����� ������
            Open ExchPath & i & OutputFile For Output As #8 '
            Close #8
            LogAction "exchange|Sinchronize|�� ������� ����������� ���� " & ExchPath & i & OutputFile
        End If
    Next i
    If PathFileExists(ExchPath & InputFile) = 1 Then
        f = FreeFile
        Open ExchPath & InputFile For Input As #f ' ��������� ���� ��� ������
        Do While Not EOF(f)  ' ������� EOF(End Of File) ���������, ��������� �� ����� �����
            Line Input #f, txt ' ������ ������ ������
            TextNotLoaded = "0|" & Piece(txt, "|", 2, 99) '���� �� ������ ��������� ���� ����, �� �� ��� �� �������� � ����
            CanDelete = True
            '������ ��� ������ � ������ ������ � ������ �����
            For i = 0 To MainForm.lstPath.ListCount - 1
                Found = False
                Open ExchPath & i & OutputFile For Input As #9 ' ��������� ���� ��� ������
                Do While (Not EOF(9))  '
                    Line Input #9, Ftxt
                    If (Ftxt = txt) Or (Ftxt = TextNotLoaded) Then
                        Found = True
                    End If
                    If Ftxt = TextNotLoaded Then
                        CanDelete = False
                    End If
                Loop
                Close #9
                Open ExchPath & i & OutputFile For Append As #9
                If Not Found Then
                    Print #9, TextNotLoaded
                    LogAction "exchange|Sinchronize|����� �������� � ������ " & TextNotLoaded
                End If
                Close #9
            Next i
            Open ExchPath & "Del" & InputFile For Append As #9
            If CanDelete = False Then
                Print #9, txt
            Else
                LogAction "exchange|Sinchronize|����� ������ �� Input " & txt
            End If
            Close #9
        Loop
        Close #f
        IsCopy = CopyFile(ExchPath & "Del" & InputFile, ExchPath & InputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
        DeleteFile ExchPath & "Del" & InputFile
    End If
    For i = 0 To MainForm.lstPath.ListCount - 1
        IsCopy = CopyFile(ExchPath & i & OutputFile, MainForm.lstPath.List(i) & OutputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
        If IsCopy <> 1 Then '��� ������ ����� ������
            'MsgBox IsCopy
        End If
        DeleteFile ExchPath & i & OutputFile
        RemoveLock MainForm.lstPath.List(i) & "Lock.txt"
        LogAction "exchange|Sinchronize|����� " & MainForm.lstPath.List(i) & "Lock.txt"
    Next i
    RemoveLock (ExchPath & "Lock.txt")
    'LogAction "exchange|Sinchronize|" & Err.Number & " " & Err.Description
    Close #9
    Close #f
End Sub

Public Sub LoadExchange()
    Dim c As Object 'New ADODB.Connection
    Dim Record As Object 'New ADODB.Recordset
    
    Set Record = CreateObject("ADODB.Recordset")
    Set c = CreateObject("ADODB.Connection")
    
    On Error GoTo ErrHandlerLE
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    LogAction "exchange|LoadExchange|������ ���������"
    If (MainForm.lstPath.ListCount < 1) Or (KolComputers = 0) Then
        LogAction "exchange|Sinchronize|��� ���������� ��� ��������"
        Exit Sub
    End If
    If SetLock(ExchPath & "Lock.txt") = False Then
        LogAction "exchange|LoadExchange|���� Lock.txt �����"
        Exit Sub
    End If
    f = FreeFile
    Open ExchPath & OutputFile For Input As #f
    Open ExchPath & "TMP" & OutputFile For Append As #9
    Record.CursorLocation = adUseClient
    Do While Not EOF(f)
        Line Input #f, txt ' ������ ������ ������
        If Piece(txt, "|", 1) = "0" Then
            TableCase = Piece(txt, "|", 3)
            LogAction "exchange|LoadExchange|�������� ���������|" & txt
            If TableCase = "MAP+" Then '���������� ������ � ������� �������� �������
                StrFind = "SELECT * FROM CLOSED_T WHERE CELL='" & Piece(txt, "|", 4) & "'"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If (Record.RecordCount > 0) And (Piece(txt, "|", 4) <> "FLOOR") Then
                    Record.MoveFirst
                    Record.Fields("UIDKOR").Value = Piece(txt, "|", 5)
                    Record.Fields("PRDATE").Value = Piece(txt, "|", 6)
                    Record.Fields("PRCODE").Value = Piece(txt, "|", 7)
                    Record.Fields("REALDATE").Value = Piece(txt, "|", 9)
                    Record.Fields("VESNETTO").Value = Piece(txt, "|", 8)
                    Record.Update
                Else
                    Record.AddNew
                    Record.Fields("CELL").Value = Piece(txt, "|", 4)
                    Record.Fields("UIDKOR").Value = Piece(txt, "|", 5)
                    Record.Fields("PRDATE").Value = Piece(txt, "|", 6)
                    Record.Fields("PRCODE").Value = Piece(txt, "|", 7)
                    Record.Fields("REALDATE").Value = Piece(txt, "|", 9)
                    Record.Fields("VESNETTO").Value = Piece(txt, "|", 8)
                    Record.Update
                End If
                Record.Close
            ElseIf TableCase = "MAP0+" Then '���������� ������ � ������� �������� �������
                StrFind = "SELECT * FROM CLOSED_T"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                Record.AddNew
                Record.Fields("CELL").Value = ""
                Record.Fields("UIDKOR").Value = Piece(txt, "|", 4)
                Record.Fields("PRDATE").Value = Piece(txt, "|", 5)
                Record.Fields("PRCODE").Value = Piece(txt, "|", 6)
                Record.Fields("VESNETTO").Value = Piece(txt, "|", 7)
                Record.Fields("REALDATE").Value = Piece(txt, "|", 8)
                Record.Update
                Record.Close
            ElseIf TableCase = "MAP-" Then '�������� ������ �� ������� �������� �������
                StrFind = "SELECT * FROM CLOSED_T WHERE UIDKOR='" & Piece(txt, "|", 4) & "'"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                    If IsNull(Record.Fields("CELL").Value) Then
                        iCell = ""
                    Else
                        iCell = Record.Fields("CELL").Value
                    End If
                    If (iCell = "") Or (iCell = "FLOOR") Then
                        Record.Delete
                    Else
                        Record.Fields("UIDKOR").Value = ""
                        Record.Fields("PRDATE").Value = ""
                        Record.Fields("PRCODE").Value = ""
                        Record.Fields("REALDATE").Value = ""
                        Record.Fields("VESNETTO").Value = ""
                    End If
                    Record.Update
                Else
                    LogAction "exchange|LoadExchange|�� ����� ������� " & Piece(txt, "|", 4)
                End If
                Record.Close
            ElseIf TableCase = "MAP~" Then '������� ��� �� �����
                StrFind = "SELECT * FROM CLOSED_T WHERE UIDKOR='" & Piece(txt, "|", 5) & "'"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                    If IsNull(Record.Fields("CELL").Value) Then
                        iCell = Piece(txt, "|", 4)
                    Else
                        iCell = Record.Fields("CELL").Value
                    End If
                    If IsNull(Record.Fields("PRCODE").Value) Then
                        iPrCode = ""
                    Else
                        iPrCode = Record.Fields("PRCODE").Value
                    End If
                    If IsNull(Record.Fields("PRDATE").Value) Then
                        iPrDate = ""
                    Else
                        iPrDate = Record.Fields("PRDATE").Value
                    End If
                    If IsNull(Record.Fields("UIDKOR").Value) Then
                        iUIDKor = ""
                    Else
                        iUIDKor = Record.Fields("UIDKOR").Value
                    End If
                    If IsNull(Record.Fields("VESNETTO").Value) Then
                        iVesNet = ""
                    Else
                        iVesNet = Record.Fields("VESNETTO").Value
                    End If
                    If IsNull(Record.Fields("REALDATE").Value) Then
                        iRealDate = ""
                    Else
                        iRealDate = Record.Fields("REALDATE").Value
                    End If
                    If (iCell = "") Or (iCell = "FLOOR") Then
                        Record.Delete
                    Else
                        Record.Fields("PRCODE").Value = ""
                        Record.Fields("PRDATE").Value = ""
                        Record.Fields("UIDKOR").Value = ""
                        Record.Fields("VESNETTO").Value = ""
                        Record.Fields("REALDATE").Value = ""
                    End If
                    Record.AddNew
                    Record.Fields("CELL").Value = ""
                    Record.Fields("PRCODE").Value = iPrCode
                    Record.Fields("PRDATE").Value = iPrDate
                    Record.Fields("UIDKOR").Value = iUIDKor
                    Record.Fields("VESNETTO").Value = iVesNet
                    Record.Fields("REALDATE").Value = iRealDate
                    Record.Update
                    'Record.Fields("CELL").Value = ""
                    'Record.AddNew
                    'Record.Fields("CELL").Value = iCell
                    'Record.Update
                Else
                    LogAction "exchange|LoadExchange|�� ����� ������� " & Piece(txt, "|", 5)
                End If
                Record.Close
            ElseIf TableCase = "TASK~" Then '���������� ������� �� ��������� �/�
                StrFind = "SELECT * FROM TASK_TAB WHERE TASKNUM='" & Piece(txt, "|", 4) & "'"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                    Record.Fields("CLCODE") = Piece(txt, "|", 5)
                    Record.Fields("RMNUM") = Piece(txt, "|", 6)
                    Record.Fields("TASKEND") = Piece(txt, "|", 7)
                    Record.Fields("FILENAME") = Piece(txt, "|", 8)
                    Record.Fields("UNLOAD") = "9"
                    Record.Update
                Else
                    LogAction "exchange|LoadExchange|�� ����� �������" & Piece(txt, "|", 4)
                End If
                Record.Close
            ElseIf TableCase = "TASK+" Then '��������� ������� �� ����������
                sqlQuery = Piece(txt, "|", 4, 99)
                c.Execute sqlQuery
            ElseIf TableCase = "OPEN+" Then '���������� ������� �� ����� �������� �������
                c.Execute "INSERT INTO OPEN_TAB values ('" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "','" & Piece(txt, "|", 9) & "')"
            ElseIf TableCase = "OPEN-" Then '�������� ������� �� ������ �������� �������
                tmpUID = Piece(txt, "|", 5)
                If tmpUID = "" Then
                    c.Execute "DELETE FROM OPEN_TAB WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND UIDBAR IS NULL AND VESNETTO='" & Piece(txt, "|", 6) & "'"
                Else
                    c.Execute "DELETE FROM OPEN_TAB WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND UIDBAR='" & Piece(txt, "|", 5) & "' AND VESNETTO='" & Piece(txt, "|", 6) & "'"
                End If
            ElseIf TableCase = "OPEN--" Then '�������� ������� �� ������ �������� �������
                c.Execute "DELETE FROM OPEN_TAB"
            ElseIf TableCase = "MAP0-" Then '�������� ������� �� ������ �������� �������
                c.Execute "DELETE FROM CLOSED_T WHERE CELL IS NULL"
            ElseIf TableCase = "OPEN~" Then '�������� ������� �� ������ �������� �������
                tmpUID = Piece(txt, "|", 5)
                If tmpUID = "" Then
                    StrFind = "SELECT * FROM OPEN_TAB WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND UIDBAR IS NULL AND VESNETTO='" & Piece(txt, "|", 6) & "'"
                Else
                    StrFind = "SELECT * FROM OPEN_TAB WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND UIDBAR='" & Piece(txt, "|", 5) & "' AND VESNETTO='" & Piece(txt, "|", 6) & "'"
                End If
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                    Record.Fields("VESNETTO").Value = Piece(txt, "|", 7)
                    Record.Update
                Else
                    LogAction "exchange|LoadExchange|�� ����� ��������"
                End If
                Record.Close
            ElseIf TableCase = "OPEN~~" Then '������ �� ������ �������� �������
                ClearOpenTable Piece(txt, "|", 4), Piece(txt, "|", 5), True
            ElseIf TableCase = "OPEND~~" Then '������ �� ������ �������� �������
                ClearOpenTableDates Piece(txt, "|", 4), Piece(txt, "|", 5), Piece(txt, "|", 6), True
            ElseIf TableCase = "REFBAR+" Then '���������� ������� � ������� ������� ��� ����������
                c.Execute "INSERT INTO REFILL_B values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "')"
            ElseIf TableCase = "COMT+" Then '���������� ������� � ������� �������
                c.Execute "INSERT INTO COMING_T values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "','" & Piece(txt, "|", 9) & "','" & Piece(txt, "|", 10) & "','" & Piece(txt, "|", 11) & "','" & Piece(txt, "|", 12) & "','" & Piece(txt, "|", 13) & "','" & Piece(txt, "|", 14) & "','" & Piece(txt, "|", 15) & "')"
            ElseIf TableCase = "REFBAR-" Then '�������� ������� �� ������� ������� ��� ����������
                c.Execute "DELETE FROM REFILL_B WHERE UIDBAR='" & Piece(txt, "|", 4) & "'"
            ElseIf TableCase = "REFTAB+" Then '���������� ������� � ������� ������� ��� ����������
                c.Execute "INSERT INTO REFILL_T values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','','')"
            ElseIf TableCase = "REFTAB-" Then '�������� ������� �� ������� ������� ��� ����������
                c.Execute "DELETE FROM REFILL_T WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND DTTMST='" & Piece(txt, "|", 5) & "'"
            ElseIf TableCase = "REFTAB~" Then '��������� ������� � ������� ������� ��� ����������
                StrFind = "SELECT * FROM REFILL_T WHERE PRCODE='" & Piece(txt, "|", 4) & "' AND DTTMST='" & Piece(txt, "|", 6) & "'"
                Record.Open StrFind, c, adOpenStatic, adLockOptimistic
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                    Record.Fields("FILLTYPE").Value = Piece(txt, "|", 5)
                    Record.Fields("DTTMEN").Value = Piece(txt, "|", 8)
                    Record.Update
                Else
                    LogAction "exchange|LoadExchange|�� ����� �������" & Piece(txt, "|", 4) & " " & Piece(txt, "|", 6)
                End If
                Record.Close
            ElseIf TableCase = "VZT+" Then '���������� ������ � ������� ���������
                c.Execute "INSERT INTO VOZVR_T values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "','" & Piece(txt, "|", 9) & "','" & Piece(txt, "|", 10) & "','" & Piece(txt, "|", 11) & "','" & Piece(txt, "|", 12) & "','" & Piece(txt, "|", 13) & "')"
            ElseIf TableCase = "VZH+" Then '���������� ������ � ��������� ���������
                c.Execute "INSERT INTO VOZVR_H values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "','" & Piece(txt, "|", 9) & "','" & Piece(txt, "|", 10) & "')"
            ElseIf TableCase = "VZH~" Then '�������� �������� �������� (3-� ���� �������� �� ������� ���������� ������ 9)
                Record.CursorLocation = adUseClient
                Record.CursorType = adOpenStatic
                Record.LockType = adLockOptimistic
                locDocNum = Piece(txt, "|", 4)
                Record.Open "SELECT * FROM VOZVR_H WHERE DOCNUM='" & locDocNum & "'", c
                If Record.RecordCount > 0 Then
                   Record.MoveFirst
                    dopdan1 = Piece(tempdopdan, "$", 1)
                    dopdan2 = Piece(tempdopdan, "$", 2)
                    Record.Fields("DOPD").Value = dopdan1 & "$" & dopdan2 & "$" & 9
                    Record.Update
                End If
                Record.Close
            ElseIf TableCase = "GOF+" Then
                c.Execute "INSERT INTO GOFRA values ('" & Piece(txt, "|", 4) & "','" & Piece(txt, "|", 5) & "','" & Piece(txt, "|", 6) & "','" & Piece(txt, "|", 7) & "','" & Piece(txt, "|", 8) & "','" & Piece(txt, "|", 9) & "')"
            ElseIf TableCase = "GOF-" Then
                c.Execute "DELETE FROM GOFRA WHERE SAVEDATE='" & Piece(txt, "|", 4) & "' AND SAVETIME='" & Piece(txt, "|", 5) & "' AND UIDKOR='" & Piece(txt, "|", 6) & "' AND TYPETAR='" & Piece(txt, "|", 7) & "'"
            ElseIf TableCase = "ACTION_T" Then
                tNumAct = Piece(txt, "|", 4)
                tKodProd = Piece(txt, "|", 5)
                tKolvo = Piece(txt, "|", 6)
                tNumOrder = Piece(txt, "|", 7)
                tDisc = Piece(txt, "|", 8)
                
                Record.CursorLocation = adUseClient
                Record.CursorType = adOpenStatic
                Record.LockType = adLockOptimistic
                Record.Open "SELECT * FROM ACTION_T WHERE NUMACT='" & tNumAct & "' AND KODPROD='" & tKodProd & "' AND DISC='" & tDisc & "'", c
                
                If Record.RecordCount > 0 Then
                    Record.MoveFirst
                                
                    tves = Record.Fields("WGHTSELL").Value
                    If IsNull(tves) Then
                        tves = 0
                    Else
                        tves = CDbl(tves)
                    End If
                         
                    tves = tves + CDbl(tKolvo)
                         
                    Record.Fields("WGHTSELL").Value = CStr(tves)
                                   
                    vesAct = Record.Fields("WGHTACT").Value
                    If IsNull(vesAct) Then
                        vesAct = 0
                    Else
                        vesAct = CDbl(vesAct)
                    End If
                     
                    tOrder = Record.Fields("NUMORDER").Value
                    If IsNull(tOrder) Then
                        tOrder = ""
                    Else
                        tOrder = CStr(tOrder)
                    End If
                     
                    tOrder = tOrder & tNumOrder & "|"
                    Record.Fields("NUMORDER").Value = tOrder
                                    
                    '�������� ����� ���� ��������� �����, ������� �������� ��� �� ����� � ���������� ��������� �����
                    If tves >= vesAct Then
                        tState = CStr(Record.Fields("STATE").Value)
                        
                        If (tState = "5") Or (tState = "1") Then
                            Record.Fields("STATE").Value = 3
                            Record.Fields("DTMSTATE").Value = CStr(Now())
                        End If
                    End If
                    
                    Record.Update
                End If
                Record.Close
            
            ElseIf TableCase = "CASH+" Then ' �������� �������� �� �����
                blokUID = Piece(txt, "|", 4)
                tipOper = Piece(txt, "|", 5)
                vidOper = Piece(txt, "|", 6)
                Summa = Piece(txt, "|", 7)
                TabNo = Piece(txt, "|", 8)
                DopDan = Piece(txt, "|", 9)
                tDate = Piece(txt, "|", 10)
                tTime = Piece(txt, "|", 11)
                If Not setCashOper(blokUID, CBool(tipOper), vidOper, CDbl(Summa), TabNo, DopDan, False, CStr(tDate), CStr(tTime)) Then ' ��� ������
                    LogAction "exchange|LoadExchange|�� ������� ��������� �������� �������� |" & blokUID & "|" & tDate & "|" & tTime, True
                End If
                
            End If
            Print #9, "1|" & Piece(txt, "|", 2, 99) '��������� ������ � �������, ��� ��� ���������
        Else
            Print #9, txt '��������� ������ ��� ���������
        End If
    Loop
    Close #f
    Close #9
    DeleteFile (ExchPath & OutputFile)
    IsCopy = CopyFile(ExchPath & "TMP" & OutputFile, ExchPath & OutputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
    If IsCopy <> 1 Then '��� ������ ����� ������
        
    End If
    c.Close
    DeleteFile (ExchPath & "TMP" & OutputFile)
    RemoveLock (ExchPath & "Lock.txt")
    Exit Sub
ErrHandlerLE:
    c.Close
    Close #f
    Close #9
    RemoveLock (ExchPath & "Lock.txt")
    LogAction "exchange|LoadExchange|" & Err.Number & " " & Err.Description, True
End Sub

Public Sub ClearOutput()
    Dim LockFile As String, i As Integer, CanDelete As Boolean
    LogAction "exchange|ClearOutput|������ ��������� ������� ����� ������ Output"
    If (MainForm.lstPath.ListCount < 1) Or (KolComputers = 0) Then
        LogAction "exchange|Sinchronize|��� ���������� ��� ��������"
        Exit Sub
    End If
    If SetLock(ExchPath & "Lock.txt") = False Then
        LogAction "exchange|ClearOutput|���� Lock.txt �����"
        Exit Sub
    End If
    For i = 0 To MainForm.lstPath.ListCount - 1
        Kol = 0
        LockFile = MainForm.lstPath.List(i) & "Lock.txt"
        If AnybodyHere(Piece(MainForm.lstPath.List(i), "\", 3)) Then
            If SetLock(LockFile) = False Then
                DeleteRemoteLocks i
                RemoveLock (ExchPath & "Lock.txt")
                Exit Sub
            End If
        Else
            RemoveLock (ExchPath & "Lock.txt")
            LogAction "exchange|ClearOutput|��� ��������� � ����|" & LockFile
            Exit Sub
        End If
        
        IsCopy = CopyFile(MainForm.lstPath.List(i) & InputFile, ExchPath & i & InputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
        If IsCopy <> 1 Then '��� ������ ����� ������
            Open ExchPath & i & OutputFile For Output As #8 '
            Close #8
            LogAction "exchange|ClearOutput|�� ������� ����������� ���� " & ExchPath & i & InputFile
        End If
    Next i
    If PathFileExists(ExchPath & OutputFile) = 1 Then
        f = FreeFile
        Open ExchPath & OutputFile For Input As #f ' ��������� ���� ��� ������
        Do While Not EOF(f)  ' ������� EOF(End Of File) ���������, ��������� �� ����� �����
            Line Input #f, txt ' ������ ������ ������
            CanDelete = True
            '������ ��� ������ � ������ ������ � ������ �����
            For i = 0 To MainForm.lstPath.ListCount - 1
                Open ExchPath & i & InputFile For Input As #9 ' ��������� ���� ��� ������
                Do While (Not EOF(9))  '
                    Line Input #9, Ftxt
                    If (Ftxt = txt) Then
                        CanDelete = False
                    End If
                Loop
                Close #9
            Next i
            Open ExchPath & "Del" & OutputFile For Append As #9
            If CanDelete = False Then
                Print #9, txt
            Else
                LogAction "exchange|ClearOutput|����� ������ �� Output " & txt
            End If
            Close #9
        Loop
        Close #f
        IsCopy = CopyFile(ExchPath & "Del" & OutputFile, ExchPath & OutputFile, False) 'False � ��������� ��������� �������� �� ���������� �����
        DeleteFile ExchPath & "Del" & OutputFile
    End If
    For i = 0 To MainForm.lstPath.ListCount - 1
        DeleteFile ExchPath & i & InputFile
        RemoveLock MainForm.lstPath.List(i) & "Lock.txt"
        LogAction "exchange|ClearOutput|����� " & MainForm.lstPath.List(i) & "Lock.txt"
    Next i
    RemoveLock (ExchPath & "Lock.txt")
    Close #9
    Close #f
End Sub

Public Sub ClearOpenTable(iPrCode As String, Optional CurSell As Double, Optional exchange As Boolean = False)   '��������� ��������� ������ �������� ������� ��� ����������
    On Error GoTo errHandlerCOT
    Dim c As Object 'New ADODB.Connection
    Dim VesStr As Double, ToTable As Double, tmpVes As String, tmpVesTable As String, tmpVesDouble As Double
    
    Set c = CreateObject("ADODB.Connection")
    
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Set Record = CreateObject("ADODB.Recordset")
    Record.CursorLocation = adUseClient
    LogAction "FormSell|ClearOpenTable|" & CStr(iPrCode) & "|" & CStr(CurSell) & "|������ �� �������� ������� ������ ����������"
    ExchFile = "Input.txt"
    If Not exchange Then
        If KolComputers > 0 Then
            AddToTextFile ExchPath & ExchFile, "1|" & IP & "|OPEN~~|" & iPrCode & "|" & CurSell & "|" & Now()
        End If
    End If
    StrFind = "SELECT * FROM OPEN_TAB WHERE PRCODE='" & iPrCode & "'"
    '�������� ��������� � ������ CursorLocation
    Record.Open StrFind, c, adOpenStatic, adLockOptimistic
    If Record.RecordCount > 0 Then
        Record.MoveFirst
        Do Until Record.EOF
            If IsNull(Record.Fields("VESNETTO").Value) Then
                VesStr = 0
                tmpVesTable = 0
            Else
                tmpVes = Record.Fields("VESNETTO").Value
                tmpVesTable = tmpVes
                If InStr(tmpVes, ".") > 0 Then tmpVes = Piece(tmpVes, ".", 1) & "," & Piece(tmpVes, ".", 2)
                VesStr = CDbl(tmpVes)
            End If
            If IsNull(Record.Fields("UIDBAR").Value) Then
                tmpUIDB = 0
            Else
                tmpUIDB = Record.Fields("UIDBAR").Value
            End If
            If VesStr <= CurSell Then
                LogAction "FormSell|ClearOpenTable|" & CStr(VesStr) & "|" & CStr(CurSell) & "|������ ������ �������"
                CurSell = CurSell - VesStr
                Record.Delete
                Record.Update
                'If Not Exchange Then
                    'AddToTextFile ExchPath & ExchFile, "1|" & MainForm.IP & "|OPEN-|" & iPrCode & "|" & tmpUIDB & "|" & tmpVesTable
                'End If
            ElseIf CurSell = 0 Then
                
            Else
                tmpVesDouble = CurSell * 1000 + 0.5
                CurSell = CLng(Piece(tmpVesDouble, ",", 1)) / 1000
                ToTable = VesStr - CurSell
                tmpVesDouble = ToTable * 1000 + 0.5
                ToTable = CLng(Piece(tmpVesDouble, ",", 1)) / 1000
                Record.Fields("VESNETTO").Value = CStr(ToTable)
                Record.Update
                CurSell = 0
                'If Not Exchange Then
                    'AddToTextFile ExchPath & ExchFile, "1|" & MainForm.IP & "|OPEN~|" & iPrCode & "|" & tmpUIDB & "|" & tmpVesTable & "|" & ToTable
                'End If
                LogAction "FormSell|ClearOpenTable|" & CStr(VesStr) & "|" & CStr(CurSell) & "|" & CStr(ToTable) & "|������� ������"
            End If
            Record.MoveNext
        Loop
    End If
    If CurSell > 0 Then
        ClearFromClosedTab iPrCode, CurSell, exchange
    End If
    Record.Close
    c.Close
    Set Record = Nothing
    Set c = Nothing
    Exit Sub
errHandlerCOT:
    LogAction "exchange|ClearOpenTable|" & Err.Number & "|" & Err.Description, True
End Sub

Public Sub ClearOpenTableDates(iPrCode As String, Optional DelDate As String = "", Optional CurSell As Double, Optional exchange As Boolean = False)  '��������� ��������� ������ �������� ������� ��� ����������
    On Error GoTo errHandlerCOT
    Dim c As Object 'New ADODB.Connection
    Dim VesStr As Double, ToTable As Double, tmpVes As String, tmpVesTable As String, tmpVesDouble As Double
    
    Set c = CreateObject("ADODB.Connection")
    
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Set Record = CreateObject("ADODB.Recordset")
    Record.CursorLocation = adUseClient
    LogAction "FormSell|ClearOpenTable|" & CStr(iPrCode) & "|" & CStr(CurSell) & "|������ �� �������� ������� ������ ����������"
    ExchFile = "Input.txt"
    If Not exchange Then
        If KolComputers > 0 Then
            AddToTextFile ExchPath & ExchFile, "1|" & IP & "|OPEND~~|" & iPrCode & "|" & DelDate & "|" & CurSell & "|" & Now()
        End If
    End If
    If DelDate = "" Then
        StrFind = "SELECT * FROM OPEN_TAB WHERE PRCODE='" & iPrCode & "' AND REALDATE IS NULL"
    Else
        StrFind = "SELECT * FROM OPEN_TAB WHERE PRCODE='" & iPrCode & "' AND REALDATE='" & DelDate & "'"
    End If
    '�������� ��������� � ������ CursorLocation
    Record.Open StrFind, c, adOpenStatic, adLockOptimistic
    If Record.RecordCount > 0 Then
        Record.MoveFirst
        Do Until Record.EOF
            If IsNull(Record.Fields("VESNETTO").Value) Then
                VesStr = 0
                tmpVesTable = 0
            Else
                tmpVes = Record.Fields("VESNETTO").Value
                tmpVesTable = tmpVes
                If InStr(tmpVes, ".") > 0 Then tmpVes = Piece(tmpVes, ".", 1) & "," & Piece(tmpVes, ".", 2)
                VesStr = CDbl(tmpVes)
            End If
            If IsNull(Record.Fields("UIDBAR").Value) Then
                tmpUIDB = 0
            Else
                tmpUIDB = Record.Fields("UIDBAR").Value
            End If
            If VesStr <= CurSell Then
                LogAction "FormSell|ClearOpenTableDates|" & CStr(VesStr) & "|" & CStr(CurSell) & "|������ ������ �������"
                CurSell = Round(CurSell - VesStr, 3)
                Record.Delete
                Record.Update
            ElseIf CurSell = 0 Then
                
            Else
                tmpVesDouble = CurSell * 1000 + 0.5
                CurSell = CLng(Piece(tmpVesDouble, ",", 1)) / 1000
                ToTable = Round(VesStr - CurSell, 3)
                tmpVesDouble = ToTable * 1000 + 0.5
                ToTable = CLng(Piece(tmpVesDouble, ",", 1)) / 1000
                Record.Fields("VESNETTO").Value = CStr(ToTable)
                Record.Update
                CurSell = 0
                LogAction "exchange|ClearOpenTableDates|" & CStr(VesStr) & "|" & CStr(CurSell) & "|" & CStr(ToTable) & "|������� ������"
            End If
            Record.MoveNext
        Loop
    Else
        If CurSell < 0 Then
            c.Execute "INSERT INTO OPEN_TAB values('" & iPrCode & "','','" & CStr(-CurSell) & "','','" & DelDate & "')"
        End If
    End If
    Record.Close
    c.Close
    Set Record = Nothing
    Set c = Nothing
    Exit Sub
errHandlerCOT:
    LogAction "exchange|ClearOpenTableDates|" & Err.Number & "|" & Err.Description, True
End Sub

Private Sub ClearFromClosedTab(iPrCode As String, Optional CurSell As Double, Optional exchange As Boolean = False)
    On Error GoTo errHandlerCCT
    Dim c As Object 'New ADODB.Connection
    
    Set c = CreateObject("ADODB.Connection")
    
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    
    Set rst = CreateObject("ADODB.Recordset")
    rst.CursorLocation = adUseClient
    StrFind = "SELECT * FROM CLOSED_T WHERE PRCODE='" & iPrCode & "' AND CELL IS NOT NULL"
    rst.Open StrFind, c, adOpenStatic, adLockReadOnly
    chCell = "": chRealDate = "": chBar = "": chVes = ""
    SravnDate = DateAdd("d", 1000, Piece(Now(), " ", 1))
    If rst.RecordCount > 0 Then
        rst.MoveFirst
        Do Until rst.EOF
            If IsNull(rst.Fields("CELL").Value) Then
                iCell = ""
            Else
                iCell = rst.Fields("CELL").Value
            End If
            If IsNull(rst.Fields("REALDATE").Value) Then
                iRealDate = ""
            Else
                iRealDate = rst.Fields("REALDATE").Value
            End If
            If IsNull(rst.Fields("UIDKOR").Value) Then
                iBar = ""
            Else
                iBar = rst.Fields("UIDKOR").Value
            End If
            If IsNull(rst.Fields("VESNETTO").Value) Then
                iVes = 0
            Else
                iVes = rst.Fields("VESNETTO").Value
            End If
            If CDate(iRealDate) < SravnDate Then
                chCell = iCell
                chRealDate = iRealDate
                chBar = iBar
                chVes = iVes
                SravnDate = iRealDate
            End If
            rst.MoveNext
        Loop
    Else
        Exit Sub
    End If
    rst.Close
    If chBar <> "" Then
        If InStr(iVes, ".") > 0 Then iVes = Piece(iVes, ".", 1) & "," & Piece(iVes, ".", 2)
        StrFind = "SELECT * FROM CLOSED_T WHERE UIDKOR='" & chBar & "'"
        rst.Open StrFind, c, adOpenStatic, adLockOptimistic
        If rst.RecordCount > 0 Then
            rst.MoveFirst
            Delta = CDbl(iVes) - CurSell
            Delta = Round(CDbl(Delta), 3)
            If Delta > 0 Then
                rst.Fields("VESNETTO").Value = CStr(Delta)
                rst.Update
            ElseIf Delta = 0 Then
                rst.Delete
                rst.Update
            Else
                rst.Delete
                rst.Update
                ClearFromClosedTab iPrCode, -CDbl(Delta), exchange
            End If
        End If
        rst.Close
    End If
    Set rst = Nothing
    c.Close
    Set c = Nothing
    Exit Sub
errHandlerCCT:
    LogAction "exchange|ClearFromClosedTab|" & Err.Number & "|" & Err.Description, True
End Sub
