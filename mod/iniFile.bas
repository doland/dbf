Attribute VB_Name = "settings"
' ��������� �������
Public ClCode As String, ClName As String, IP As String

' ���� ����, ������ � �����
Public DBpath As String, ExchPath As String, LogPath As String

' ����� ������ ��� ������
Const InputFile As String = "input.txt"
Const OutputFile As String = "output.txt"

' ��� ������
Public KolComputers As Integer

' ��������� MUMPS �����������
Public MServer As String, MPort As String, MTom As String, MKip As String, IM As String

' ��������� COM �����
Public ComPort As String, ComSettings As String

Public Ref As String

'Dim SevenZPath As String, PDFReader As String
'Dim ViewClient As Boolean, LineSent As Long, Version As String
'Dim NewOwnFile As String

'Public ReFillKg As Double, ReFillSht As Integer, ConstDelta As Double, cntTmOst As Integer, naklPath As String
'Public BlankPath As String, Refill As Boolean, RMNum As String

Public Sub IniFile() '(iniF As String)
    Dim i As Integer
    Dim FNum As Long
    Dim TmpStr As String
    Dim CmpStr As String
    Dim ValStr As String
    On Error GoTo errHandler
    
    PDFReader = "":
    LogPath = App.path
    iniF = "C:\MClib\mesto.ini"
    i = 1
    FNum = FreeFile: ViewCl = 0
    Open iniF For Input As #FNum
    Do While Not EOF(FNum)
       Line Input #FNum, TmpStr
       i = i + 1
       If Left(LTrim(TmpStr), 1) = ";" Or Len(Trim(TmpStr)) = 0 Then GoTo N '������� ������������ ;
       CmpStr = Trim(Piece(TmpStr, "=", 1))
       ValStr = Trim(Piece(Piece(Piece(TmpStr, "=", 2), "[", 2), "]", 1))
       
       Select Case CmpStr
       Case "MVol":         MVol = CStr(ValStr)
       Case "MUCI":         MUCI = CStr(ValStr)
       Case "IM":           IM = CStr(ValStr)
       Case "COM":          ComPort = ValStr
       Case "COMSettings":  ComSettings = ValStr
       Case "ClCode":       ClCode = CStr(ValStr)
       Case "RMNum":        RMNum = CStr(ValStr)
       Case "DBPath":       DBpath = CStr(ValStr)
       Case "BlankPath":    BlankPath = CStr(ValStr)
       Case "7zPath":       SevenZPath = CStr(ValStr)
       Case "ViewCl":       ViewCl = CStr(ValStr)
       Case "PDFReader":    PDFReader = CStr(ValStr)
       Case "MServer":      MServer = CStr(ValStr)
       Case "MPort":        MPort = CStr(ValStr)
       Case "MTom":         MTom = CStr(ValStr)
       Case "MKip":         MKip = CStr(ValStr)
       Case "Refill":       Ref = CStr(ValStr)
       Case "ExchangeFiles":    ExchPath = CStr(ValStr)
       Case Else
       End Select
N:
    Loop
    
    Close FNum

    
    If DBpath = "" Then LogAction "IniFile|DBpath|�� �������� �������� � mesto.ini", True
    If ClCode = "" Then LogAction "IniFile|ClCode|�� �������� �������� � mesto.ini", True
    Exit Sub
errHandler:
    LogAction "IniFile|" & Err.Number & "|" & Err.Description, True
End Sub

