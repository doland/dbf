Attribute VB_Name = "cashMethods"
Public Const adUseNone = 1
Public Const adUseServer = 2

Public Const adOpenUnspecified = -1
Public Const adOpenForwardOnly = 0
Public Const adOpenKeyset = 1
Public Const adOpenDynamic = 2

Public Const adLockPessimistic = 2
Public Const adLockBatchOptimistic = 4
Public Const adLockUnspecified = -1
' by doland
' ������� ���������� ������� � ����� �� ��������� ����, ��� �� ������� ������, ��� ������ ���������� �������� -1
Public Function getCashOst(Optional tDate As String = "") As Double
    On Error GoTo ErrHandlerSR
    Dim c As Object 'New ADODB.Connection
    Set Record = CreateObject("ADODB.Recordset")
   
    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Record.CursorLocation = adUseClient

    If tDate = "" Then
        tDate = Piece(Now(), " ", 1)
    End If
    getCashOst = 0
    sqlQuery = "SELECT * FROM CASH_OST WHERE DTMSM <= " + CStr(zzTT(tDate)) + " ORDER BY DTMSM"
    Record.Open sqlQuery, c, adOpenStatic, adLockReadOnly
    
    
    If Record.RecordCount > 0 Then
        Record.MoveLast
        getCashOst = Format(CDbl(Record.Fields("OST").Value), "0.00")
    End If
    Record.Close: c.Close: Set Record = Nothing: Set c = Nothing
    Exit Function
    
ErrHandlerSR:
    getCashOst = -1
    Set Record = Nothing: Set c = Nothing
    LogAction "cashMethods|getCashOst|" & Err.Number & " " & Err.Description, True
End Function

' ������������ ���������
' blokUID - ��� ���������
' tipOper - ��� ��������
'       True- ������ � �����
'       False- ������
' vidOper - ���� ��������, ���������� ��������:
'       "PR"- ������� (����)
'       "VZ"- ������� ����������
'       "PTT"- ������ �� (������ ������� ������ � ��.)
'       "RTT"- ������� �� (������ ������� �� �����: ����������)
'       "INV"- �������������� (��������� ����� �������)
' Summa - ����� ��������
' tabNo - ����� ���������
' DopDan - �������������� ����������
' flObmen - ���� = True - �� �������� �������� � �����
'                   (���� �������� ����, ��� �������� �������� ������� � �������- ����� �� �����)
Public Function setCashOper(ByVal blokUID As String, ByVal tipOper As Boolean, ByVal vidOper As String, ByVal Summa As Double, ByVal TabNo As String, ByVal DopDan As String, Optional flObmen As Boolean = True, Optional operDate As String = "", Optional operTime As String = "") As Boolean
    Dim c As Object 'New ADODB.Connection
    Dim tekOst As Double
    Dim tDate As String, tDateMSM As String, appendRow As String, sqlQuery As String
    On Error GoTo ErrHandlerSR

    ' �������� ������������� ��������� vidOper
    If blokUID = "" Then
        LogAction "cashMethods|setCashOper|������� ������ �������� blokUID"
        setCashOper = False
        Exit Function
    
    End If
    
    blokUID = Left(blokUID, 15)
    DopDan = Left(DopDan, 10)
    TabNo = Left(TabNo, 20)
    
    ' �������� ������������� ��������� vidOper
    Select Case vidOper
        
        Case "PR", "VZ", "PTT", "RTT", "INV", "ENC"
       
        Case Else
            LogAction "cashMethods|setCashOper|����������� ������� �������� vidOper"
            setCashOper = False
            Exit Function
    
    End Select
    
    ' ������� ���� � �����
    tDate = Piece(Now(), " ", 1): tTime = Piece(Now(), " ", 2)
    
    If operDate <> "" Then ' ���� �������� ���� �����, �������� ������ �������
        tTime = Left(operTime, 8)
    End If
    
    tDateMSM = CStr(zzTT(tDate))
    
    ' �������� ������� �������
    tekOst = getCashOst
    If tekOst = -1 Then
        GoTo errGetOst:
    End If
    
    Select Case tipOper
        Case True
            tekOst = tekOst + Abs(Summa)
            Summa = Abs(Summa)
        Case False
            tekOst = tekOst - Abs(Summa)
            Summa = -Abs(Summa)
    End Select
    Summa = Format(Summa, "0.00")
    tekOst = Format(tekOst, "0.00")

    ' ���������� � ������� �������� �� ����� CASH_TRA
    Set c = CreateObject("ADODB.Connection")
    c.Open ("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & DBpath & ";User ID=Admin;Password=;Extended Properties=dBase IV")
    Set Record = CreateObject("ADODB.Recordset")
    Record.CursorLocation = adUseClient
    
    appendRow = "INSERT INTO CASH_TRA " _
                & "values ('" _
                                & tDateMSM & "','" _
                                & CStr(blokUID) & "','" _
                                & CStr(tTime) & "'," _
                                & tipOper & ",'" _
                                & CStr(Summa) & "','" _
                                & CStr(vidOper) & "','" _
                                & CStr(TabNo) & "','" _
                                & CStr(DopDan) & "','" _
                                & "')"
    c.Execute appendRow
    
    ' ������ ���� �������� ������� ������� � ������� �������� CASH_OST
    '' ������� � ������� ������� ������� ������ � ����� �����
    sqlQuery = "SELECT * FROM CASH_OST WHERE DTMSM = " + tDateMSM
    Record.Open sqlQuery, c, adOpenStatic, adLockOptimistic

    If Record.RecordCount = 0 Then      ' ��� ��� ������ �� ���� ����, �������
        sqlQuery = "INSERT INTO CASH_OST " _
                    & "values ('" _
                                & tDateMSM + "','" _
                                & tDate + "','" _
                                & CStr(tekOst) _
                                & "')"
        c.Execute sqlQuery
    ElseIf Record.RecordCount = 1 Then  ' ���� 1 ������- ��������� ��
        Record.MoveFirst
        Record.Fields("OST").Value = tekOst
        Record.Update
    Else                                ' ������� ������ ��� 1, ��������� �� ������
        LogAction "cashMethods|setCashOper|� ������� CASH_OST ������� ��������� �������� �� ���� " + CStr(tDate)
        setCashOper = False
        GoTo m1
    End If
    appRow = "1|" & IP & "|CASH+|" _
            & CStr(blokUID) & "|" & tipOper & "|" & CStr(vidOper) & "|" & CStr(Summa) & "|" _
            & CStr(TabNo) & "|" & CStr(DopDan) & "|" & CStr(tDate) & "|" & CStr(tTime)
    
    LogAction "cashMethods|setCashOper|�������� �������� " & appRow
    
    If flObmen Then
        ' �������� �������� � �����
        If KolComputers > 0 Then
            SetLock ExchPath & "Lock.txt"
            AddToTextFile ExchangePath & ExchangeFile, CStr(appRow)
            LogAction "cashMethods|setCashOper|�������� " & CStr(appRow) & " ��������� � �����"
            RemoveLock ExchPath & "Lock.txt"
            Sinchronize
        End If
    End If
    
    setCashOper = True
m1:
    Record.Close: c.Close: Set Record = Nothing: Set c = Nothing
    Exit Function

errGetOst:
    setCashOper = False
    LogAction "cashMethods|setCashOper|������ ��������� �������� �������", True
    Exit Function

ErrHandlerSR:
    Set Record = Nothing: Set c = Nothing

    setCashOper = False
    LogAction "cashMethods|setCashOper|" & Err.Number & " " & Err.Description, True
End Function
