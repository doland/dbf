Attribute VB_Name = "logFile"
' by doland

Public LogPath As String

Public Sub AddToTextFile(FileName As String, Text As String)
    On Error GoTo errHandlerATF
    f = FreeFile
    Open FileName For Append As #f
    Print #f, Text
    Close #f
    Exit Sub
errHandlerATF:

End Sub

Public Sub LogAction(Stroka As String, Optional IsError As Boolean = False, Optional Device As Integer = 0, Optional DeviceStroka As String)
    Dim FileName As String
    Dim FileNumber As String
    Dim CurDate As String, CurHour As String
    
    CurDate = Piece(Now(), " ", 1)
    CurHour = Piece(Piece(Now(), " ", 2), ":", 1)
    FileNumber = Replace(CurDate, ".", "") & CurHour

    
    If IsError Then
        FileName = CStr("" & LogPath & "LogErr" & FileNumber & ".txt")
        AddToTextFile FileName, CStr(Now() & "|" & Stroka)
    Else
        FileName = CStr("" & LogPath & "Log" & FileNumber & ".txt")
        AddToTextFile CStr(FileName), CStr(Now() & "|" & Stroka)
    End If
    
    If Device = 1 Then '������
        FileName = CStr("" & App.Path & "\Scanner.txt")
        AddToTextFile FileName, DeviceStroka
    ElseIf Device = 2 Then '����
        FileName = CStr("" & App.Path & "\Weight.txt")
        AddToTextFile FileName, DeviceStroka
    End If
    
End Sub

Public Sub MakeLogsDirectories()
    On Error Resume Next
    FileUtils_MkDir (dbPath & "Logs")
    On Error Resume Next
    FileUtils_MkDir (dbPath & "Logs\" & Replace(Piece(Now(), " ", 1), ".", ""))
    LogPath = dbPath & "Logs\" & Replace(Piece(Now(), " ", 1), ".", "") & "\"
End Sub


