Attribute VB_Name = "settings"
' ��������� �������
Public ClCode As String, ClName As String, Ip As String, RMNum As String
Public TabNo As String
 
' ���� ����, ������ � �����
Public dbPath As String, exchPath As String, LogPath As String

' ����� ������ ��� ������
Const InputFile As String = "input.txt"
Const OutputFile As String = "output.txt"

' ��� ������
Public KolComputers As Integer

' ��������� MUMPS �����������
Public MServer As String, MPort As String, MTom As String, MKip As String, IM As String

' ��������� COM ����� �������
Public ComPort As String, ComSettings As String
' ��������� COM ����� ��������
Public ComPrint As String, ComPrintSet As String
' ��������� COM ����� �����
Public VesiPort As String

Public Ref As String

Public Timer1min As Long

Public SevenZPath As String, PDFReader As String
Private Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long

Public Sub IniFile() '(iniF As String)
    Dim i As Integer
    Dim FNum As Long
    Dim TmpStr As String
    Dim CmpStr As String
    Dim ValStr As String
    On Error GoTo errHandler
    
    PDFReader = "":
    LogPath = App.Path
    iniF = "C:\MClib\mesto.ini"
    i = 1
    FNum = FreeFile: ViewCl = 0
    Open iniF For Input As #FNum
    Do While Not EOF(FNum)
       Line Input #FNum, TmpStr
       i = i + 1
       If Left(LTrim(TmpStr), 1) = ";" Or Len(Trim(TmpStr)) = 0 Then GoTo N '������� ������������ ;
       CmpStr = Trim(Piece(TmpStr, "=", 1))
       ValStr = Trim(Piece(Piece(Piece(TmpStr, "=", 2), "[", 2), "]", 1))
       
       Select Case CmpStr
       Case "MVol":         MVol = CStr(ValStr)
       Case "MUCI":         MUCI = CStr(ValStr)
       Case "IM":           IM = CStr(ValStr)
       Case "MServer":      MServer = CStr(ValStr)
       Case "MPort":        MPort = CStr(ValStr)
       Case "MTom":         MTom = CStr(ValStr)
       Case "MKip":         MKip = CStr(ValStr)
       
       Case "ClCode":       ClCode = CStr(ValStr)

       Case "RMNum":        RMNum = CStr(ValStr)

       Case "BlankPath":    BlankPath = CStr(ValStr)
       Case "7zPath":       SevenZPath = CStr(ValStr)
       Case "ViewCl":       ViewCl = CStr(ValStr)
       Case "PDFReader":    PDFReader = CStr(ValStr)

       Case "Refill":       Ref = CStr(ValStr)
       Case "ExchangeFiles":    exchPath = CStr(ValStr)
       Case "DBPath":       dbPath = CStr(ValStr)
       
       Case "COM":          ComPort = UCase(CStr(ValStr))
       Case "COMSettings":  ComSettings = CStr(ValStr)
       Case "ComPrint":     ComPrint = UCase(CStr(ValStr))
       Case "ComPrintSet":  ComPrintSet = CStr(ValStr)
       Case "VesiPort":     VesiPort = CInt(ValStr)


       Case Else
       End Select
N:
    Loop
    
    Close FNum
    
    If dbPath = "" Then LogAction "settings|DBpath|�� �������� �������� � mesto.ini", True
    If ClCode = "" Then LogAction "settings|ClCode|�� �������� �������� � mesto.ini", True
    Exit Sub
errHandler:
    LogAction "settings|" & Err.Number & "|" & Err.Description, True
End Sub

Public Sub loadSettings()
    Dim M As Object
    On Error GoTo errHandler
    Version = App.Major & "." & App.Minor & "." & App.Revision
    FileName = "C:\MClib\checker.ini"
    
    Set M = CreateObject("MC.Class1")
    
    Init = M.�������������(MServer, MPort, "USER", "1")
    If M.����������(MKip, MTom) = 0 Then
        LogAction "loadSettings|�� ������� ����������� � �������!", True
    End If
    
    ret = M.���������������("CHECKSET^DOLGOV", 6, 6, IM, ClCode, Version, RMNum & "|" & Main.Winsock1.LocalIP)
    
    If M.MErrNum <> 0 Then
        LogAction "loadSettings|��������� �������� Checker |" & M.MErrNum & "|" & M.MErrDesc, True
    Else
        If Piece(ret, "|", 1) <> "+" Then
            LogAction "loadSettings|��������� �������� Checker |" & ret, True
        Else
            If PathFileExists(FileName) = 1 Then
                DeleteFile (FileName)
            End If
            f = FreeFile
            Open FileName For Append As #f
            
            While M.���������������() = 1
                
                Print #f, M.��������������
            Wend
            Close #f
        End If
    End If
    Set M = Nothing
    Timer1min = 0
    If PathFileExists(FileName) = 1 Then
        f = FreeFile
        Open FileName For Input As #f
        Do While Not EOF(f)
            Line Input #f, Stroka
            If UCase(Piece(Stroka, "|", 1)) = "1TIMER" Then Timer1min = CDbl(Piece(Stroka, "|", 2))
        Loop
        Close #f
    End If
    
    Exit Sub
errHandler:
    Set M = Nothing
    LogAction "loadSettings|" & Err.Number & "|" & Err.Description, True
End Sub

