Attribute VB_Name = "dataMSM"
' by doland
Option Explicit
Dim DateCompare As String
Const DateConst As Date = "01.01.2010"

Public Function zzTT(DT As String) As Long
    If DT = "" Then
        Exit Function
    End If
    zzTT = CDate(DT) - CDate("01.01.2010") + 61727
End Function

Public Function zzFT(dtMSM As Long) As String
    If dtMSM < 0 Then
        Exit Function
    End If
    zzFT = CStr(DateAdd("d", dtMSM - 61727, CDate("01.01.2010")))
End Function

Public Function FT(InpDate As Integer) As String
    DateCompare = DateAdd("d", InpDate, DateConst)
    FT = CStr(DateCompare)
End Function

Public Function TT(InpDate As String) As Integer
    DateCompare = DateDiff("d", DateConst, InpDate)
    TT = CInt(DateCompare)
End Function
