VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form zScaner 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin MSCommLib.MSComm MSComm1 
      Left            =   1560
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327680
      DTREnable       =   -1  'True
   End
End
Attribute VB_Name = "zScaner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)


Private myForm As Object

Public Sub SetParent(par As Object)
    Set myForm = par
End Sub

Private Sub Form_Load()
    
    If InStr(ComPort, "COM") > 0 Then
        m_ComPort = Piece(Piece(ComPort, "COM", 2), ":", 1)
    End If
    
    MSComm1.CommPort = m_ComPort
    MSComm1.settings = ComSettings
    
    If MSComm1.PortOpen Then
        MSComm1.PortOpen = False
    End If
    
    If MSComm1.PortOpen Then
        LogAction "zScaner|Form_Load|MSComm1.PortOpen|���� ������� �����", True
        Exit Sub
    End If
    
    MSComm1.RThreshold = 1
    On Error Resume Next
    MSComm1.PortOpen = True
    If Not MSComm1.PortOpen Then
        LogAction "zScaner|Form_Load|MSComm1.PortOpen|������ ����������� �������", True
    Else
        LogAction "zScaner|Form_Load|MSComm1.PortOpen|������ ���������"
    End If
   
End Sub

Private Sub MSComm1_OnComm()
    On Error GoTo ErrHandlerOC
    Dim Data As String
    Dim strInput As String
    Dim BytesReceived() As Byte
    Set scaner = New scaner
    Sleep 100
    If MSComm1.CommEvent = comEvReceive Then
        InBuffer = MSComm1.Input         ' Pass the Input data to the InBuffer
        BytesReceived() = InBuffer       ' Pass the Byte data to the Byte array
        Data = BytesReceived()           ' Display ASCII in Text1
    End If
    
    scaner.scanLet = Data
    Exit Sub
ErrHandlerOC:
    LogAction "zScaner|MSComm1_OnComm|" & Err.Number & "|" & Err.Description, True
End Sub
