VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "scaner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim inpStr As String
Public Event scanInput(ByVal inputed As String)

Public Property Let scanLet(inpStr As String)
    If inpStr <> "" Then
        RaiseEvent scanInput(inpStr)
    End If
End Property

Public Property Get scanGet() As String
    scanGet = inpStr
End Property
